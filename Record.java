package genomicSequencing;

import java.util.*;

/**
 * Record type for string hash table
 *
 * A record associates a certain string (the key value) with
 * a list of sequence positions at which that string occurs
 *
 */
public class Record {
    private final String key;
    private final ArrayList<Integer> positions;
    private int hashedKeyValue;
    private int index;
    private boolean dirtyBit;
    /**
     * Constructs a Record with the given string
     *
     * @param s key of the Record
     */
    public Record(String s) {
        key = s;
        positions = new ArrayList<Integer>(1);
        hashedKeyValue = 0;
        index =  0;
        dirtyBit = false;
    }
    
    public int gethashedKeyValue(){
    	return hashedKeyValue;
    }
    
    public void sethashedKeyValue(int new_val){
    	this.hashedKeyValue = new_val;
    }
    
    public int getIndex(){
    	return index;
    }
    
    public void setIndex(int new_index){
    	this.hashedKeyValue = new_index;
    }
    public boolean getDirtyBit(){
    	return dirtyBit;
    }
    
    public void setDirtyBit(boolean b){
    	this.dirtyBit = b;
    }
    
    
    /**
     * Returns the key associated with this Record.
     */
    public String getKey() {
        return key;
    }

    /**
     * Adds a new position to the positions list.
     *
     * @param position of the key
     */
    public void addPosition(Integer position) {
        positions.add(position);
    }

    /**
     * @return The number of positions we have for this key.
     */
    public int getPositionsCount() {
        return positions.size();
    }

    /**
     * @return the Integer position at index
     */
    public Integer getPosition(int index) {
        return positions.get(index);
    }
}